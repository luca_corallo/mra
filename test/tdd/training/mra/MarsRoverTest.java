package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;


public class MarsRoverTest {

	@Test
	public void planetShouldContainsObstacle() throws Exception {
		//Arrange
		int x = 10;
		int y = 10;
		String[] planetObstacleArray = {"(4,7)", "(2,3)"};
		List<String> planetObstacle;
		MarsRover marsRover;
		
		//Act		
		planetObstacle = new ArrayList<>(Arrays.asList(planetObstacleArray));
		marsRover = new MarsRover(x, y, planetObstacle);
		
		//Assert
		assertTrue(marsRover.planetContainsObstacleAt(4, 7));
		assertTrue(marsRover.planetContainsObstacleAt(2, 3));
		assertFalse(marsRover.planetContainsObstacleAt(1, 3));
	}
	
	@Test
	public void roverShouldBeInStartPosition() throws Exception {
		//Arrange
		int x = 10;
		int y = 10;
		MarsRover marsRover;
		String result;
		
		//Act
		marsRover = new MarsRover(x, y, null);
		result = marsRover.executeCommand("");
		
		//Assert
		assertEquals("(0,0,N)", result);
	}
	
	@Test
	public void roverFromStartShouldChangeDirectionAfterRotationRight() throws Exception {
		//Arrange
		int x = 10;
		int y = 10;
		MarsRover marsRover;
		String result;
		
		//Act
		marsRover = new MarsRover(x, y, null);
		result = marsRover.executeCommand("r");
		
		//Assert
		assertEquals("(0,0,E)", result);
	}
	
	@Test
	public void roverFromStartShouldChangeDirectionAfterRotationLeft() throws Exception {
		//Arrange
		int x = 10;
		int y = 10;
		MarsRover marsRover;
		String result;
		
		//Act
		marsRover = new MarsRover(x, y, null);
		result = marsRover.executeCommand("l");
		
		//Assert
		assertEquals("(0,0,W)", result);
	}
	
	@Test
	public void roverThatRotate4TimesShouldHaveSameDirection() throws Exception {
		int x = 10;
		int y = 10;
		MarsRover marsRover;
		String result;
		
		//Act
		marsRover = new MarsRover(x, y, null);
		marsRover.executeCommand("l");
		marsRover.executeCommand("l");
		marsRover.executeCommand("l");
		result = marsRover.executeCommand("l");
		
		
		//Assert
		assertEquals("(0,0,N)", result);
	}
	
	@Test
	public void roverFromStartGoOn() throws Exception {
		//Arrange
		int x = 10;
		int y = 10;
		MarsRover marsRover;
		String result;
		
		//Act
		marsRover = new MarsRover(x, y, null);
		result = marsRover.executeCommand("f");
		
		//Assert
		assertEquals("(0,1,N)", result);
	}
	
	@Test
	public void roverFromStartGoBack() throws Exception {
		//Arrange
		int x = 10;
		int y = 10;
		MarsRover marsRover;
		String result;
		
		//Act
		marsRover = new MarsRover(x, y, null);
		result = marsRover.executeCommand("f");
		
		//Assert
		assertEquals("(0,1,N)", result);
	}
	
	@Test
	public void roverShouldGoBack() throws Exception {
		//Arrange
		int x = 10;
		int y = 10;
		MarsRover marsRover;
		String result;
		
		//Act
		marsRover = new MarsRover(x, y, null);
		marsRover.executeCommand("r");
		marsRover.executeCommand("r");
		result = marsRover.executeCommand("b");
		
		//Assert
		assertEquals("(0,1,S)", result);
	}
	
	@Test
	public void roverShouldFollowMoreCommand() throws Exception {
		//Arrange
		int x = 10;
		int y = 10;
		MarsRover marsRover;
		String result;
		
		//Act
		marsRover = new MarsRover(x, y, null);
		result = marsRover.executeCommand("ffrff");
		
		//Assert
		assertEquals("(2,2,E)", result);
	}
	
	@Test
	public void planetShouldBeASphere() throws Exception {
		//Arrange
		int x = 10;
		int y = 10;
		MarsRover marsRover;
		String result;
		
		//Act
		marsRover = new MarsRover(x, y, null);
		result = marsRover.executeCommand("b");
		
		//Assert
		assertEquals("(0,9,N)", result);
	}
	
	@Test
	public void roverShouldEncourageAnObstacle() throws Exception {
		//Arrange
		int x = 10;
		int y = 10;
		String[] planetObstacleArray = {"(2,2)"};
		MarsRover marsRover;
		String result;
		
		//Act
		marsRover = new MarsRover(x, y, new ArrayList<String>(Arrays.asList(planetObstacleArray)));
		result = marsRover.executeCommand("ffrfff");
		
		//Assert
		assertEquals("(1,2,E)(2,2)", result);
	}

	@Test
	public void roverShouldEncourageMoreObstacles() throws Exception {
		//Arrange
		int x = 10;
		int y = 10;
		String[] planetObstacleArray = {"(2,2)", "(2,1)"};
		MarsRover marsRover;
		String result;
		
		//Act
		marsRover = new MarsRover(x, y, new ArrayList<String>(Arrays.asList(planetObstacleArray)));
		result = marsRover.executeCommand("ffrfffrflf");
		
		//Assert
		assertEquals("(1,1,E)(2,2)(2,1)", result);
	}
	
	@Test
	public void roverShouldEncourageObstacleBeyondAnEdge() throws Exception {
		//Arrange
		int x = 10;
		int y = 10;
		String[] planetObstacleArray = {"(0,9)"};
		MarsRover marsRover;
		String result;
		
		//Act
		marsRover = new MarsRover(x, y, new ArrayList<String>(Arrays.asList(planetObstacleArray)));
		result = marsRover.executeCommand("b");
		
		//Assert
		assertEquals("(0,0,N)(0,9)", result);
	}
	
	@Test
	public void roverShouldDoACorrectTour() throws Exception {
		//Arrange
		int x = 6;
		int y = 6;
		String[] planetObstacleArray = {"(2,2)", "(0,5)", "(5,0)"};
		MarsRover marsRover;
		String result;
		
		//Act
		marsRover = new MarsRover(x, y, new ArrayList<String>(Arrays.asList(planetObstacleArray)));
		result = marsRover.executeCommand("ffrfffrbbblllfrfrbbl");
		
		//Assert
		assertEquals("(0,0,N)(2,2)(0,5)(5,0)", result);
	}
}
