package tdd.training.mra;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MarsRover {

	private int planetX;
	private int planetY;
	
	private List<String> direction;
	private String dir;
	private int posX;
	private int posY;

	private List<String> planetObstacles;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		direction = new ArrayList<>(Arrays.asList(new String[]{ "W", "N", "E", "S"}));
		this.planetX = planetX;
		this.planetY = planetY;
		dir = "N";
		posX = 0;
		posY = 0;
		this.planetObstacles = planetObstacles;
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		int xObs;
		int yObs; 
		for(String obstacle: planetObstacles) {
			xObs = Integer.valueOf(String.valueOf(obstacle.charAt(1)));
			yObs = Integer.valueOf(String.valueOf(obstacle.charAt(3)));
			if(xObs == x && yObs == y)
				return true;
		}
		return false;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		int tempX, tempY;
		boolean resultAction;
		String obstacles = "";
		for(char cmd : commandString.toCharArray()) {
			tempX = posX;
			tempY = posY;
			
			executeOneCommand(cmd);
			resultAction = thereIsAnObstacle();
			
			if(!resultAction) {
				obstacles = fixRover(obstacles);
				posX = tempX;
				posY = tempY;
			}
		}
		return "(" + posX + "," + posY + "," + dir + ")" + obstacles;
	}
	
	private String getDirectionAfterRotation(int sum) {
		int offsetDirection = direction.indexOf(dir) + sum;
		if(offsetDirection < 0) offsetDirection = 3;
		else if(offsetDirection > 3) offsetDirection = 0;
		return direction.get(offsetDirection);
	}
	
	private void roverGoOn(int sum) {
		switch(dir) {
			case "N":
				posY += sum;
			break;
			case "W":
				posX -= sum;
			break;
			case "E":
				posX += sum;
			break;
			case "S":
				posY -= sum;
			break;
		}
		posX = fixOutOfBound(posX, planetX);
		posY = fixOutOfBound(posY, planetY);
	}
	
	private void executeOneCommand(char command) {
		switch(command) {
			case 'l':
				dir = getDirectionAfterRotation(-1);
			break;
			case 'r':
				dir = getDirectionAfterRotation(+1);
			break;
			case 'f':
				roverGoOn(1);
			break;
			case 'b':
				roverGoOn(-1);
			break;
		}
	}
	
	private int fixOutOfBound(int pos, int planet) {
		if(pos < 0) pos = planet - 1;
		else if(pos == planet) pos = 0;
		return pos;
	}
	
	private boolean thereIsAnObstacle() {
		try {
			if(planetObstacles != null && planetContainsObstacleAt(posX, posY)) return false;
			return true;
			
		} catch(MarsRoverException e) {
			return false;
		}
	}
	
	private String fixRover(String obstacles) {
		String newObstacle = "("+posX+","+posY+")";
		if(!obstacles.contains(newObstacle))
			obstacles += newObstacle;
		return obstacles;
	}
}
